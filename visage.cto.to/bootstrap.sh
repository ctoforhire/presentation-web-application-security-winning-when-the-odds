#!/usr/bin/env bash

DEBIAN_FRONTEND=noninteractive aptitude update
DEBIAN_FRONTEND=noninteractive aptitude install -y vim libapache2-mod-php5 php5 php5-cli php5-mcrypt php5-mysql mariadb-client mariadb-server

sed -i 's#/var/www/html#/var/www#g' /etc/apache2/sites-enabled/000-default.conf
service apache2 restart

if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant/www /var/www
fi
